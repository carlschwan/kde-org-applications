<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

include 'includes/class_appdata2.inc';
$app = new AppData2($_GET['application']);
if ($app->valid() === false) {
    http_response_code(404);
    print 'App not found';
    exit();
}

if (strpos($_GET['application'], 'org.kde') === false) {
    header("Location: /applications/" . $_GET['category'] . '/' . 'org.kde.' . $_GET['application']);
    exit();
}

$category = $_GET['category'];
$development = isset($_GET['development']);

$index = json_decode(file_get_contents("index.json"), true);

$categories = array_keys($index);
foreach($categories as $cat) {
    if (strtolower($cat) == $_GET["category"]) {
        $category_string = $cat;
        break;
    }
}
if (!isset($category_string)) {
    http_response_code(404);
    print 'Category not found';
    exit();
}


// if app category is wrong forward to correct one
$correct_category = null;
foreach ($categories as $cat) {
    if (in_array($_GET['application'], $index[$cat])) {
        $correct_category = $cat;
    }
}
if (strtolower($correct_category) != $_GET['category']) {
    header("Location: /applications/" . strtolower($correct_category) . '/' . $_GET['application']);
    exit();
}

$page_title = $app->name();
if (!empty($app->genericName())) {
    $page_title = $page_title." - ".$app->genericName();
}

if ($development) {
    $page_title=$app->name()." - Development Information";
}
$page_title_extra_html = '<div class="app-icon"><img src="/applications/icons/'.$app->icon().'" alt="'.$app->name().' Icon" /></div>';

function printSidebar($app, $category)
{
    $content = '';

    if ($app->hasHomepage() || $app->hasKDEApps()) {
        $content .= '<div class="infobox"><strong>More about '.$app->name().'</strong>';
        if ($app->hasHomepage()) {
            $content .= '<br /><a href="'.$app->homepage().'">'.$app->name().' Homepage</a>';
        }
        if ($app->hasKDEApps()) {
            $url = htmlspecialchars("http://kde-apps.org/content/show.php?content=".$app->KDEAppsId());
            $content .= '<br /><a href="'.$url.'">'.$app->name().' on KDE-Apps.org</a>';
        }
        $content .= '</div>';
    }

    $content .= '<div class="infobox"><strong>Get help</strong>';
    if ($app->hasUserbase()) {
        $content .= '<br /><a href="'.$app->userbase().'">'.$app->name().' on UserBase</a>';
    }
    $content .= '<br /><a href="'.$app->forumUrl().'">KDE Community Forums</a>';
    if ($app->hasHandbook()) {
        $content .= '<br /><a href="'.$app->handbook().'">'.$app->name().' Handbook</a>';
    }
    $content .= '</div>';

    $content .= '<div class="infobox"><strong>Contact the authors</strong>';

    //Bug tracker links
    if ($app->hasBugTracker()) {
        if ($app->isBugTrackerExternal()) {
            $content .= '<br /><a href="'.htmlentities($app->bugzillaProduct()).'">Report a bug</a>';
        } else { //KDE Bugzilla
            $componentstring = "";
            if ($app->bugzillaComponent()) {
                $componentstring = '&amp;component='.$app->bugzillaComponent();
            }
            $content .= '<br /><a href="https://bugs.kde.org/enter_bug.cgi?format=guided&amp;product='.$app->bugzillaProduct().$componentstring.'">Report a bug</a>';
        }
    } else { //Empty bugtracker, use default link
        $content .= '<br /><a href="https://bugs.kde.org/wizard.cgi">Report a bug</a>';
    }
    foreach ($app->ircChannels() as $channel) {
        $content .= '<br />IRC: <a href="irc://irc.freenode.org/'.$channel.'">'.$channel.' on Freenode</a>';
    }
    foreach ($app->mailingLists() as $ml) {
        //KDE mailing lists
        if (substr($ml, -8, 8) == "@kde.org") {
            $base = substr($ml, 0, -8);
            $content .= '<br />Mailing List: <a href="mailto:'.htmlspecialchars($ml).'">' .htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="https://mail.kde.org/mailman/listinfo/'.$base.'">subscribe</a>, <a href="https://mail.kde.org/mailman/listinfo/'.$base.'/">list information</a>)';
        } else if (substr($ml, -22, 22) == "@lists.sourceforge.net") { //Sourceforge.net
            $base = substr($ml, 0, -22);
            $content .= '<br />Mailing List: <a href="mailto:'.htmlspecialchars($ml).'">'.htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="https://lists.sourceforge.net/lists/listinfo/'.$base.'">subscribe</a>, <a href="http://sourceforge.net/mailarchive/forum.php?forum_name='.$base.'">archive</a>)';
        } else if (substr($ml, 0, 31) == "http://groups.google.com/group/") { //Google Groups (web)
            $base = substr($ml, 31, strlen($ml)-31);
            $content .= '<br />Mailing List: <a href="mailto:'.htmlspecialchars($base).'@googlegroups.com">'.htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="http://groups.google.com/group/'.$base.'/subscribe?note=1">subscribe</a>, <a href="http://groups.google.com/group/'.$base.'/topics">archive</a>)';
        } else if (substr($ml, -17, 17) == "@googlegroups.com") { //Google Groups (mail)
            $base = substr($ml, 0, -17);
            $content .= '<br />Mailing List: <a href="mailto:'.htmlspecialchars($ml).'">'.htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="http://groups.google.com/group/'.$base.'/subscribe?note=1">subscribe</a>, <a href="http://groups.google.com/group/'.$base.'/topics">archive</a>)';
        } else { //Default mail
            $content .= '<br />Mailing List: <a href="mailto:'.htmlspecialchars($ml).'">'.$app->name().'</a>';
        }
    }
    $content .= '</div>';

    $content .= '<div class="infobox"><strong><a href="'.$_SERVER['REQUEST_URI'].'/development">Development Information</a></strong></div>';

    $content .= <<<EOT
        <link href="/applications/fonts/css/fontawesome-all.min.css" rel="stylesheet">
        <link rel="stylesheet" href="/applications/download.css">

        <div class="infobox"><strong>Get {$app->name()}</strong>
            <div class="downloadButton">
                <a href="appstream://{$app->AppStreamId()}">
                    <span class="fa fa-download"></span>
                    Install
                </a>
            </div>
        </div>
EOT;

    return $content;
}

function printPage($app)
{
    //Print screenshot or dummy "no screenshot available" image
    $thumbUrl = $app->defaultScreenshotThumbnailUrl();
    $screenshotUrl = $app->defaultScreenshotUrl();
    if ($screenshotUrl) {
        print '<div class="app-screenshot"><a href="'.$screenshotUrl.'">
        <img src="'.$thumbUrl.'" width=540 alt="Screenshot" />
        </a></div>'; ///TODO: image size
    } else {
        print '<div class="app-screenshot">
        <img src="/images/screenshots/no_screenshot_available.png" alt="No screenshot available" />
        </div>'; ///TODO: image size
    }

    print $app->descriptionHtml();
    if ($app->hasVersions() || $app->hasAuthors() || $app->hasLicense()) {
        if ($app->hasAuthors()) {
            print '<h2>Developed By</h2>';
            print $app->authorHtml();
        }
        if ($app->hasVersions()) {
            ///TODO: Versions not yet implemented
        }
        if ($app->hasLicense()) {
            print '<h2>License</h2>';
            print $app->licenseHtml();
        }
    }

    print '<div style="clear:left;"></div>';
}

function printDevelopmentPage($app)
{
    print '<h2>Source Code Repository</h2>';

    print $app->browseSourcesHtml();

    print $app->checkoutSourcesHtml();

    //Show bugzilla related links only for applications hosted at bugs.kde.org
    if ($app->hasBugTracker() && !$app->isBugTrackerExternal()) {
        print '<h2>Search for open bugs</h2>';

        $product = $app->bugzillaProduct();

        $componentstring = "";
        if ($app->bugzillaComponent()) {
            $componentstring = '&component='.$app->bugzillaComponent();
        }

        $majorBugs  = 'https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash';
        $minorBugs  = 'https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=normal&bug_severity=minor';
        $wishes     = 'https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=wishlist';
        $juniorJobs = 'https://bugs.kde.org/buglist.cgi?keywords=junior-jobs&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&cmdtype=doit';

        print '<ul>';
        print '<li><a href="'.htmlspecialchars($majorBugs).'">Major Bug reports</a></li>';
        print '<li><a href="'.htmlspecialchars($minorBugs).'">Minor Bug reports</a></li>';
        print '<li><a href="'.htmlspecialchars($wishes).'">Wish reports</a></li>';
        print '<li><a href="'.htmlspecialchars($juniorJobs).'">Junior Jobs</a></li>';
        print '</ul>';
    }

    if ($app->hasEbn()) {
        print '<h2>Code checking</h2>';
        print '<p>Show results of automated code checking on the English Breakfast Network (EBN).</p>';
        print '<ul>';
        print '<li><a href="'.htmlspecialchars($app->ebnCodeCheckingUrl()).'">Code Checking</a></li>';
        print '<li><a href="'.htmlspecialchars($app->ebnDocCheckingUrl()).'">Documentation Sanitation</a></li>';
        print '</ul>';
    }

    print '<br clear="all"/>';
}


require('config.php');
require(KDE_ORG . '/aether/config.php');

$pageConfig = array_merge($pageConfig, [
    'title' => $page_title,
    'cssFile' => '/css/applications.css'
]);

require(KDE_ORG . '/aether/header.php');
$site_root = "../";


echo '<main class="container">';

echo '<h1><a href="/applications/">KDE\'s Applications</a> <a href="/applications/'.$category.'">'.$category_string.'</a> '. $app->name() .'</h1>';

echo '<div id="sidebar">';
echo printSidebar($app, $category);
echo '</div>';

if (!$development) {
    printPage($app);
} else {
    printDevelopmentPage($app);
}

echo '</main>';
require(KDE_ORG . '/aether/footer.php');
