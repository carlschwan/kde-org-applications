<?php

/**
* Reads a json file containing information for an application page on kde.org and gives
* access to its data.
* Written by Daniel Laidig <d.laidig@gmx.de>
* Rebased on new backend data by Harald Sitter <sitter@kde.org>
*
* Usage:
* <?php
*  TODO
* ?>
*/

class AppData2
{
    var $data;

    function AppData2($name, $path = "..") {
        $name = preg_replace('/[^a-z\-_0-9\.]/i','',$name);
        $file_path = $path.'/applications/appdata/'.$name.'.json';
        $this->file_path = $file_path;

        // Make sure to resolve symlinks to their actual name. Specifically
        // symlinks may be used for backwards compatibility of old names, but
        // data resolution should happen on the current name.
        set_error_handler(function() { /* ignore errors */ });
        $link = readlink($file_path);
        restore_error_handler();
        if ($link != FALSE) {
            $file_path = $link;
            $name = basename($file_path, '.json');
        }
        if (!file_exists($file_path)) {
            http_response_code(404);
            return false;
        }

        $this->data = json_decode(file_get_contents($file_path), true);
        $extension_file = $path.'/applications/appdata-extensions/'.$name.'.json';
        if (file_exists($extension_file)) {
            $this->data = array_merge_recursive($this->data, json_decode(file_get_contents($extension_file), true));
        }

        $this->id = $name;
    }

    // Does this app exist?
    function valid() {
        if (!file_exists($this->file_path)) {
            return false;
        }
        return true;
    }

    # Same as AppStreamId but with .desktop stripped to force consistent naming.
    function id() {
        if (empty($this->id)) {
            return $this->data['X-KDE-ID'];
        }
        return $this->id;
    }

    function name() {
        return $this->l10n($this->data['Name']);
    }

    function genericName() {
// FIXME: summary is a bad fit for this as it can be rather long
        // return $this->l10n($this->data['Summary']);
        return $this->l10n($this->data['X-KDE-GenericName']);
    }

    // FIXME: in appdata things can be in more than one category, the sturcture of the site is kinda only expecting things once
    function category() {
        return $this->data['Categories'][0];
    }

    function get_client_prefered_language($getSortedList = false, $acceptedLanguages = false)
    {
        if (empty($acceptedLanguages)) {
            $acceptedLanguages = $_SERVER["HTTP_ACCEPT_LANGUAGE"];
        }
        // print_r($acceptedLanguages); print_r("<br/>");

        // regex inspired from @GabrielAnderson on http://stackoverflow.com/questions/6038236/http-accept-language
        preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})*)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $acceptedLanguages, $lang_parse);
        $langs = $lang_parse[1];
        $ranks = $lang_parse[4];

        // (create an associative array 'language' => 'preference')
        $lang2pref = array();
        for($i=0; $i<count($langs); $i++) {
            $lang2pref[$langs[$i]] = (float) (!empty($ranks[$i]) ? $ranks[$i] : 1);
        }

        // (comparison function for uksort)
        $cmpLangs = function ($a, $b) use ($lang2pref) {
            if ($lang2pref[$a] > $lang2pref[$b])
                return -1;
            elseif ($lang2pref[$a] < $lang2pref[$b])
                return 1;
            elseif (strlen($a) > strlen($b))
                return -1;
            elseif (strlen($a) < strlen($b))
                return 1;
            else
                return 0;
        };

        // sort the languages by prefered language and by the most specific region
        uksort($lang2pref, $cmpLangs);

        if ($getSortedList) {
            return array_keys($lang2pref);
        }

            // return the first value's key
        reset($lang2pref);
        return key($lang2pref);
    }

    function l10n($ary) {
        if (sizeof($ary) == 0) {
           //print "Empty l10n array";
           return;
        }
        static $__c_alternates = array('en', 'en-US');
        $ret = $ary['C'];
        foreach($this->get_client_prefered_language(True) as $lang) {
            // print_r($lang); print_r("<br/>");
            if (in_array($lang, $__c_alternates, true)) {
                return $ret; // Defaults to C already.
            }
            if (array_key_exists($lang, $ary)) {
                return $ary[$lang];
            }
        }
        return $ret;
    }

    function descriptionHtml() {
        $description = $this->l10n($this->data['Description']);

        // Force paragraphing if the Description doesn't have it.
        if(substr($description, 0, 3) != '<p>') {
            $description = '<p>'.$description.'</p>';
        }

        return $description;
    }

    function hasVersions() {
        return false; ///TODO: version information is not yet implemented
    }

    function hasAuthors() {
        return isset($this->data['authors']);
    }

    function authorHtml() {
        $html = '<p><strong>'.i18n_var( "Authors:" ).'</strong></p>';
        $html .= '<ul>';
        foreach($this->data['authors'] as $author) {
            $html.= '<li><span>'.$author[0].'</span>';
            if (strlen($author[2])) {
                $html .= ' <a href="mailto:'.htmlspecialchars($author[2]).'">&lt;'.htmlspecialchars($author[2]).'&gt;</a>';
            }
            $html.='<br />'.i18n_var($author[1]);
            if (strlen($author[3])) {
                $html .= '<br /><a href="'.htmlspecialchars($author[3]).'">'.htmlspecialchars($author[3]).'</a>';
            }
            $html .= '</li>';
        }
        $html .= '</ul>';
        if (count($this->data['credits'])) {
            $html .= '<p><strong>'.i18n_var( "Thanks To:" ).'</strong></p>';
            $html .= '<ul>';
            foreach($this->data['credits'] as $author) {
                $html.= '<li><span>'.$author[0].'</span>';
                if (strlen($author[2])) {
                    $html .= ' <a href="mailto:'.htmlspecialchars($author[2]).'">&lt;'.htmlspecialchars($author[2]).'&gt;</a>';
                }
                $html.='<br />'.i18n_var($author[1]);
                if (strlen($author[3])) {
                    $html .= '<br /><a href="'.htmlspecialchars($author[3]).'">'.htmlspecialchars($author[3]).'</a>';
                }
                $html .= '</li>';
            }
            $html .= '</ul>';
        }
        return $html;
    }

    function hasLicense() {
        return isset($this->data['ProjectLicense']);
    }

    function licenseHtml() {
        // TODO: we should really render SPDX properly. There are some databases
        //   of licenses and their texts. Or we could send URLs to spdx.org.
        $license = $this->data['ProjectLicense'];
        $text = '<p>';
        // $stripped_license = str_replace("+", "", $license);
        // $text = i18n_var('%1 is distributed under the terms of the <script type="text/javascript">document.write(spdxToHTML("%3"))</script></a>.',
        //                  $this->name(), $stripped_license, $license);
        switch($license) {
        case 'GPL-2.0+': case 'GPL-2.0': case 'GPL-2.0-or-later': case 'GPL-2.0-only':
            $text .= i18n_var('%1 is distributed under the terms of the <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html">GNU General Public License (GPL), Version 2</a>.', $this->name());
            break;
        case 'GPL-3.0+': case 'GPL-3.0': case 'GPL-3.0-or-later': case 'GPL-3.0-only':
            $text .= i18n_var('%1 is distributed under the terms of the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License (GPL), Version 3</a>.', $this->name());
            break;
        case 'LGPL-2.0-only': case 'LGPL-2.0-or-later': case 'LGPL-2.1-only': case 'LGPL-2.1-or-later': case 'LGPL':
            $text .= i18n_var('%1 is distributed under the terms of the <a href="http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html"> GNU Lesser General Public License, version 2</a>.', $this->name());
            break;
        case 'LGPL-3.0-only': case 'LGPL-3.0-or-later': case 'LGPL-3.0':
            $text .= i18n_var('%1 is distributed under the terms of the <a href="https://www.gnu.org/licenses/lgpl.html"> GNU Library General Public License, version 3</a>.', $this->name());
            break;
        default:
            $text .= $license.'.';
        }
        $text .= '</p>';
        return $text;
    }

    function hasHomepage() {
        return isset($this->data['Url']['homepage']);
    }

    function homepage() {
        return $this->data['Url']['homepage'];
    }

    function hasAppStream() {
        return true;
    }

    function AppStreamId() {
        return $this->data['ID'];
    }

    function icon() {
        return $this->data['Icon']['local'][0]['name'];
    }

// TODO: not in appdata
    function hasKDEApps() {
        return isset($this->data['kde-apps.org']);
    }
// FIXME: not in appdata
    function KDEAppsId() {
        return $this->data['kde-apps.org'];
    }
// FIXME: not in appdata
    function hasUserbase() {
        return isset($this->data['userbase']);
    }
// FIXME: not in appdata
    function userbase() {
        return "http://userbase.kde.org/".$this->data['userbase'];
    }

    function hasHandbook() {
        return isset($this->data['Url']['help']);
    }
    function handbook() {
        return $this->data['Url']['help'];
    }

    // AppData extension X-KDE-Forum: <String>
    function forumUrl() {
        if (isset($this->data['X-KDE-Forum'])) {
            return 'http://forum.kde.org/viewforum.php?f='.$this->data['X-KDE-Forum'];
        } else {
            return 'http://forum.kde.org/';
        }
    }

    // AppData extension X-KDE-IRC: <String, Array>
    function ircChannels() {
        if (isset($this->data['X-KDE-IRC'])) {
            $irc = $this->data['X-KDE-IRC'];
            if (is_array($irc)) {
                return $irc;
            } else {
                return array($irc);
            }
        } else {
            return array("#kde");
        }
    }

    // AppData extension X-KDE-MailingLists: <String, Array>
    function mailingLists() {
        if (isset($this->data['X-KDE-MailingLists'])) {
            $ml = $this->data['X-KDE-MailingLists'];
            if (is_array($ml)) {
                return $ml;
            } else {
                return array($ml);
            }
        } else {
            return array("kde@kde.org");
        }
    }

    // AppData extension X-KDE-Repository: <String>
    function repository() {
        return $this->data['X-KDE-Repository'];
    }

    function browseSourcesHtml() {
        $path = '<p><a href="https://cgit.kde.org/'.$this->repository();
        $path .= '">'.i18n_var("Browse %1 source code online", $this->name()).'</a></p>';
        return $path;
    }

    function checkoutSourcesHtml() {
        return '<p>'.i18n_var("Clone %1 source code:",
                              $this->name()).'</p><p><code>git clone https://anongit.kde.org/'.$this->repository().'</code></p>';
    }

    //Returns true if the application has a bugtracker (external or KDE bugzilla)
    function hasBugTracker() {
        return isset($this->data['Url']['bugtracker']);
    }

    function bugTracker() {
        return $this->data['Url']['bugtracker'];
    }

// FIXME: not implemented url types in frontend:
// faq
// donation
// translate

    // TODO: appstream bts are always complete URLs so this code is pointless
    // Returns true if the application has an external bugtracker (!bugs.k.o)
    function isBugTrackerExternal() {
        return (substr($this->bugTracker(), 0, 7) == "http://" ||
        substr($this->bugTracker(), 0, 8) == "https://");
    }

    function bugzillaProduct() {
        if (is_array($this->bugTracker())) {
            return $this->bugTracker()[0];
        }
        return $this->bugTracker();
    }

    function bugzillaComponent() {
        if (is_array($this->bugTracker())) {
            return $this->bugTracker()[1];
        }
        return false;
    }

    // TODO: ebn is broken for now
    function hasEbn() {
        return false;
    }

    function ebnUrlBase() {
        // examples:
        // KDE/kdeedu/parley:               http://englishbreakfastnetwork.org/krazy/reports/kde-4.x/kdeedu/parley/index.html
        // KDE/kdebase/apps/dolphin:        http://englishbreakfastnetwork.org/krazy/reports/kde-4.x/kdebase-apps/dolphin/index.html
        // extragear/graphics/digikam:      http://englishbreakfastnetwork.org/krazy/reports/extragear/graphics/digikam/index.html
        // koffice/kword:                   http://englishbreakfastnetwork.org/krazy/reports/bundled-apps/koffice/kword/index.html
        // amarok (gitorious):              http://englishbreakfastnetwork.org/krazy/reports/extragear/multimedia/amarok-git/index.html

        $ebn = '';
        // extract path segments from module parent
        $parts = explode('/', $this->data['parent']);

        // replace path segment by "kde-4.x" for "kde"
        if ($parts[0] == 'kde') {
            $ebn = "kde-4.x/" . $parts[1] . "/" . strtolower($this->data['repository'][1]);
        } else {
            $ebn = $parts[0] . "/" . $parts[1] . "/" . strtolower($this->name());
        }
        return $ebn;
    }

    function ebnCodeCheckingUrl()
    {
        return 'http://ebn.kde.org/krazy/reports/'.$this->ebnUrlBase().'/index.html';
    }

    function ebnDocCheckingUrl()
    {
        return 'http://ebn.kde.org/sanitizer/reports/'.$this->ebnUrlBase().'/index.html';
    }

    function defaultScreenshotUrl() {
        // TODO: implement l10n
        if (!array_key_exists('Screenshots', $this->data)) {
            return NULL;
        }
        foreach($this->data['Screenshots'] as $screenshot) {
            if ($screenshot['default'] == true && $screenshot['source-image']['lang'] == 'C') {
                return $screenshot['source-image']['url'];
            }
        }
        // No default found, use first available screenshot.
        foreach($this->data['Screenshots'] as $screenshot) {
            if ($screenshot['source-image']['lang'] == 'C') {
                return $screenshot['source-image']['url'];
            }
        }
        return NULL;
    }

    function defaultScreenshotThumbnailUrl() {
        // TODO: implement l10n
        if (!array_key_exists('Screenshots', $this->data)) {
            return $this->defaultScreenshotUrl();
        }
        foreach($this->data['Screenshots'] as $screenshot) {
            if ($screenshot['default'] == true && $screenshot['source-image']['lang'] == 'C') {
                return '/applications'.$screenshot['thumbnails'][0]['url'];
            }
        }
        // No default found, use first available screenshot.
        foreach($this->data['Screenshots'] as $screenshot) {
            if ($screenshot['source-image']['lang'] == 'C') {
                return '/applications'.$screenshot['thumbnails'][0]['url'];
            }
        }
        // If we can't find a thumb, simply use the off-site real image and let
        // the browser scale it.
        return $this->defaultScreenshotUrl();
    }
}
