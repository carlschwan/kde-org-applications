<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require('config.php');
require(KDE_ORG . '/aether/config.php');

$pageConfig = array_merge($pageConfig, [
    'title' => "KDE's Applications",
    'cssFile' => '/css/applications.css'
]);

require(KDE_ORG . '/aether/header.php');
require('includes/class_appdata2.inc');

$site_root = "../";

echo '<main class="container">';

echo '<h1><a href="/applications/">KDE\'s Applications</a></h1>';

$index = json_decode(file_get_contents("index.json"), true);

$categories = array_keys($index);
sort($categories);
/*
$apps = array();
foreach($categories as $category) {
    foreach($index[$category] as $application) {
        $apps[] = $application;
    }

    echo "<p class=\"app-category\">
    <a href=\"/applications/".strtolower($category)."/\" >
    <img width=\"48\" height=\"48\" src=\"icons/categories/".strtolower($category).".svg\" alt=\"".$category."\" title=\"".$category."\"/>
        $category
    </a></p>\n";
}
$apps = array_values(array_unique($apps)); # throw out dupes but get new keys
*/
foreach($categories as $category) {

    // category header
    $icon = "icons/categories/".strtolower($category);
    // special case unmaintained to an unmaintained icon
    if ($category == 'unmaintained') {
        $icon = "/applications/icons/org.kde.blogilo";
        echo '<p id="app-additional-info-paragraph"></p>';
        echo '<div id="app-additional-info">';
    }
    echo "<h2 style='margin-left: 2ex; font-size: x-large; font-weight: bold;'><a href=\"/applications/".strtolower($category)."/\" >
    <img width=\"48\" height=\"48\" src=\"".$icon.".svg\" alt=\"".$category."\" title=\"".$category."\"/>
        $category
    </a></h2>\n";

    // all the apps in the category
    foreach($index[$category] as $application) {
        $app = new AppData2($application);

        echo "<p class=\"app-category\">

        <a href=\"/applications/".strtolower($category)."/$application\" style='font-weight: normal'>
            <img width=\"48\" height=\"48\" src=\"/applications/icons/".$app->icon()."\" alt=\"".$app->name()."\" title=\"".$app->name()."\" />
            ".$app->name()."
        </a>
        <br />
        ".$app->genericName()."</p>\n";
    }

    if ($category == 'unmaintained') {
        echo '</div>';
        echo '<script src="toggle.js" defer></script>';
    }
    echo '<div style="clear: left;"><br /></div>';
}


function nameToUrl($s)
{
    return str_replace(' ', '', strtolower($s));
}


// Like str_replace but only replacing first occurance.
function str_replace_first($from, $to, $subject)
{
    $from = '/'.preg_quote($from, '/').'/';

    return preg_replace($from, $to, $subject, 1);
}

function addUrlToName($text, $appName, $url)
{
    return str_replace_first($appName, '<a href="'.$url.'"><strong>'.$appName.'</strong></a>', $text);
}

/*
$randomNumber = mt_rand(0, count($apps) - 1);
$app = new AppData2($apps[$randomNumber]);

$appurl = nameToUrl($app->category()).'/'.nameToUrl($app->id());

echo '<h2>Application Spotlight: '.addUrlToName($app->name(), $app->name(), $appurl).'</h2>';
print addUrlToName($app->descriptionHtml(), $app->name(), $appurl);

// FIXME: this is fairly duped from applicationpage2 (was before I got to it)
$thumbUrl = $app->defaultScreenshotThumbnailUrl();
$screenshotUrl = $app->defaultScreenshotUrl();
if ($screenshotUrl) {
    print '<div class="app-screenshot"><a href="'.$screenshotUrl.'">
    <img src="'.$thumbUrl.'" width=540 alt="Screenshot" />
    </a></div>'; ///TODO: image size
} else {
    print '<div class="app-screenshot">
    <img src="/images/screenshots/no_screenshot_available.png" alt="No screenshot available" />
    </div>'; ///TODO: image size
}
*/

echo '</main>';
require(KDE_ORG . '/aether/footer.php');
